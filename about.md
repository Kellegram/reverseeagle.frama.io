---
layout: post
title: ReverseEagle - About
author: ReverseEagle
---

# About us

> "There is an [...] experience which I am certain that everyone in this room has had. It entails an individual who, thinking they're alone, engages in some expressive behavior — wild singing, gyrating dancing, some mild sexual activity — only to discover that, in fact, they are not alone, that there is a person watching and lurking, the discovery of which causes them to immediately cease what they were doing in horror. The sense of shame and humiliation in their face is palpable. It's the sense of, **this is something I'm willing to do only if no one else is watching.**"

— [Glenn Greenwald, 2014 (Ted Talk)](https://www.ted.com/talks/glenn_greenwald_why_privacy_matters/transcript)

There are many people who, [unintentionally](https://www.isaca.org/resources/isaca-journal/past-issues/2012/lack-of-privacy-awareness-in-social-networks) or not, are handing over their personal and private information, be it their conversations, their photographs, personally identifiable information, or otherwise sensitive data, to malicious corporations. This data is not only held by these companies, but [sold to advertisers](https://www.bbc.com/news/technology-46618582), the [US government](https://www.theatlantic.com/technology/archive/2013/04/do-you-want-the-government-buying-your-data-from-corporations/275431/), or anybody who has such capital.

"[Analytics](https://en.wikipedia.org/wiki/Analytics)" is a name given to analysis of data or a set of statistics. It's the most common way of collecting, storing and processing data that has once belonged to someone. For years it has plagued a majority of websites and applications, without giving users sufficient notice or ability to disable said tools.

Nowadays, protecting your private life online can feel like an impossible task, but many steps can be taken to get closer and closer to this dream, where you can decide on what part of your life is public and what should stay with you.

This is precisely why ReverseEagle was created. We want to take that extra step forward, and create a more private, free, and healthy internet, and software that you can trust; taking the proprietary parts of [free and open source (FOSS)](https://en.wikipedia.org/wiki/Free_and_open-source_software) as our first target. Too many projects that claim to be a bastion of privacy have been overtaken by the influence of corporations. The whole idea of FOSS is being torn apart by addition of proprietary bits.

Our short-term goal as a group is to help out fellow developers with removing OR replacing the unnecessary proprietary code in their open source projects. Whether by submitting [merge requests](https://codeberg.org/ReverseEagle/DeGoogle-FOSS/src/branch/master/MERGE_REQUESTS.md) ourselves, or by raising [issues](https://codeberg.org/ReverseEagle/DeGoogle-FOSS/src/branch/master/ISSUES.md) in the repositories. The end goal is for the project to be free of closed source software and of any direct or indirect connection with monolithic and centralized corporations.