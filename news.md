---
layout: post
title: ReverseEagle - News
author: taminaru
---

# DeGoogled projects

Currently there are a total of 10 projects that have fully migrated from Google:

- [Merge Requests](https://codeberg.org/ReverseEagle/DeGoogle-FOSS/src/branch/master/MERGE_REQUESTS.md#user-content-list-of-merge-requests)
- [Issues](https://codeberg.org/ReverseEagle/DeGoogle-FOSS/src/branch/master/ISSUES.md#user-content-list-of-issues)

# News

## 29-06-2020

Our website is now built using [Eleventy](https://11ty.io), a static site generator. This will ease our work and make contribution to the website easier.

## 20-06-2020

Our [Lemmy](https://dev.lemmy.ml/c/reverseeagle) has reached 50 subscribers! We are glad to have every single one of you taking part in this mission!
