---
layout: post
title: ReverseEagle - [matrix] rules
author: ReverseEagle
---

# [matrix] Rules

### Rule 1

Be respectful and kind to one another despite having a disagreement. Discussions are tolerated but toxic arguments are not. Furthermore, slurs of any kind, shape or form are not tolerated. Phobic speeches are also completely prohibited.

### Rule 2

No lewd, nude or sensitive images and videos allowed.

### Rule 3

This is a serious room, so we will appreciate if the topics discussed are related to ReverseEagle's goal. You are free to create your own unofficial room with your own sets of rules under the name of ReverseEagle. If you create a room for the project, please tell the admins so they can make a decision to if they should add your room or not. Your room will be presented in the <a href="../unofficial-rooms/">list of unofficial rooms</a> once it gets accepted.

### Rule 4

This is <b>not</b> a political room. Leave politics outside of this room as much as possible.

### Rule 5

This is an English speaking room, so please converse in English only. Topics about other languages are allowed and tolerated, but not conversing.

<a href="https://matrix.to/#/!TDiDZaaFzkUSHRWcxQ:chat.endl.site?via=chat.endl.site&via=matrix.org" target="_blank" rel="noopener">[matrix] invite link</a>
